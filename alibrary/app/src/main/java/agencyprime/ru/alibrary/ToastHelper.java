package agencyprime.ru.alibrary;

import android.content.Context;
import android.widget.Toast;

import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by zaxarikovaaa on 17.01.17.
 */

public class ToastHelper {
    public static void showToast(Context context, final String message) {
        Observable.create(Observer::onCompleted)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(res -> {},
                        error -> {},
                        () -> Toast.makeText(context, message, Toast.LENGTH_SHORT).show());

    }
}
